<?php

namespace App\Form;

use App\Entity\Reclamation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
 

class ReclamationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('objet',TextType::class, [
                'attr' =>['class'=>'form-control','placeholder'=>'Objet de votre reclamation']
            ])
            ->add('motif',TextareaType::class , [
                'attr' =>['class'=>'form-control', 'placeholder'=>'motif']
                ])
            ->add('matiere',TextType::class , [
                'attr' =>['class'=>'form-control', 'placeholder'=>'Matiere']
                ])
            ->add('professeur',TextType::class , [
                'attr' =>['class'=>'form-control', 'placeholder'=>'Prof']
                ])
          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reclamation::class,
        ]);
    }
}
