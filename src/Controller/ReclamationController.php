<?php

namespace App\Controller;

use App\Entity\Reclamation;
use App\Form\ReclamationType;
use App\Repository\ReclamationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/reclamation")
 * 
 */
class ReclamationController extends AbstractController 
{
    
    /**
     * @Route("/", name="reclamation_index", methods={"GET"})
     * 
     */
    public function index(ReclamationRepository $reclamationRepository): Response
    {
        if ($this->getUser()->getRoles()[0]=="ROLE_ADMIN"){

            return $this->render('reclamation/index.html.twig', [
                'reclamations' => $reclamationRepository->findAll(),
            ]);
        }
        else{
            return $this->redirectToRoute('reclamation_indexEtud');

        }

    }
      /**
     * @Route("/indexEtud", name="reclamation_indexEtud", methods={"GET"})
     * 
     */
    public function indexEtud(ReclamationRepository $reclamationRepository): Response
    {
       
            return $this->render('reclamation/index.html.twig', [
                'reclamations' => $reclamationRepository->findByUser($this->getUser()),
            ]);
      

    }



    //   /**
    //  * @Route("/confirmation", name="reclamation_confirmation")
    //  * @IsGranted("ROLE_USER")
    //  */
    // public function confirmation(): Response
    // {
    //     $reclamation=$this->getUser()->getReclamation();
       
    //      $message="Réclamation envoyé avec succes !";
    //     return $this->render('reclamation/confirmation.html.twig',[
    //         'message' =>$message,
    //         'reclamation'=>$reclamation
    //     ]);
    // }


    /**
     * @Route("/new", name="reclamation_new", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * 
     */
    public function new(Request $request): Response
    {
        $reclamation = new Reclamation();
        $form = $this->createForm(ReclamationType::class, $reclamation);
        $form->handleRequest($request);
        $message="Réclamation envoyé avec succes !";

        if ($form->isSubmitted() && $form->isValid()) {

                $reclamation->setUser($this->getUser());

                $reclamation->setDateCreation($reclamation->getDateCreation());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reclamation);
            $entityManager->flush();
           // confirmation($reclamation);

         //   return $this->redirectToRoute('reclamation_confirmation');
         return $this->render('reclamation/confirmation.html.twig', [
            'reclamation' => $reclamation,
            'message' =>$message,

        ]);
        }

        return $this->render('reclamation/new.html.twig', [
            'reclamation' => $reclamation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reclamation_show", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function show(Reclamation $reclamation): Response
    {
        return $this->render('reclamation/show.html.twig', [
            'reclamation' => $reclamation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="reclamation_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Reclamation $reclamation): Response
    {
        $form = $this->createForm(ReclamationType::class, $reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamation_index');
        }

        return $this->render('reclamation/edit.html.twig', [
            'reclamation' => $reclamation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reclamation_delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     */
    public function delete(Request $request, Reclamation $reclamation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reclamation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reclamation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('reclamation_index');
    }

    
  
}
