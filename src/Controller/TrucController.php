<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TrucController extends AbstractController
{
    /**
     * @Route("/", name="truc")
     */
    public function index(): Response
    {
        return $this->render('truc/index.html.twig', [
           // 'controller_name' => 'YVONNAJUUF',
        ]);
    }

     /**
     * @Route("/propos", name="truc_propos")
     */
    public function propos(): Response
    {
        return $this->render('truc/propos.html.twig');
    }
}

